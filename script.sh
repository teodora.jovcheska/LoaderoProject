pip3 install virtualenv
virtualenv --version
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt
python3 main.py
